require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get inicio" do
    get :inicio
    assert_response :success
  end

  test "should get legal" do
    get :legal
    assert_response :success
  end

  test "should get contacto" do
    get :contacto
    assert_response :success
  end

  test "should get privacidad" do
    get :privacidad
    assert_response :success
  end

  test "should get blog" do
    get :blog
    assert_response :success
  end

  test "should get archivos" do
    get :archivos
    assert_response :success
  end

end
